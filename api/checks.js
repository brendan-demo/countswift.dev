const db = require('../db');

const lyrics = [
    'I wish you were a better man',
    'And I realize the blame is on me',
    'And she thinks I\'m psycho \'cause I like to rhyme her name with things',
    'He built a fire just to keep me warm',
    'You are the best thing that\'s ever been mine',
    'Darling, I\'m a nightmare dressed like a daydream',
    'So casually cruel in the name of being honest'
];

async function vibeCheck(req, res, next) {
    res.json({
        taylorsays: lyrics[Math.floor(Math.random() * lyrics.length)]
    });
}

async function dbCheck(req, res, next) {
    const time = await db.getTime();
    res.send(time);
}

module.exports = { vibeCheck, dbCheck };
