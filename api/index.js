var express = require('express');
var router = express.Router();

const albums = require('./albums.js');
const checks = require('./checks');

/* ROOT */
router.get('/', (req, res) => res.send("Ok"));
router.get('/version', (req, res) => res.send("1.0.0"));
router.get('/vibe-check', checks.vibeCheck);
router.get('/db-check', checks.dbCheck);

/* ALBUMS */
router.get('/albums', albums.getAlbums);

module.exports = router;
