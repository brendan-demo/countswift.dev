const db = require('../db')

async function getAlbums(req, res, next) {
  const albums = await db.albums.getAllAlbums();
  res.json(albums);
}

module.exports = { getAlbums };
