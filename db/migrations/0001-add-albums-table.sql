CREATE TABLE albums(
    id SERIAL PRIMARY KEY  NOT NULL,
    name TEXT,
    description TEXT,
    release_date DATE,
    album_art TEXT
)