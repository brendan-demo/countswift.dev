INSERT INTO albums (name,description,release_date,album_art) VALUES
	 ('Taylor Swift',NULL,'2006-10-24','Taylor_Swift_-_Taylor_Swift.png'),
	 ('Fearless',NULL,'2008-11-11','Taylor_Swift_-_Fearless.png'),
	 ('Speak Now',NULL,'2010-10-25','Taylor_Swift_-_Speak_Now_cover.png'),
	 ('Red',NULL,'2012-10-22','Taylor_Swift_-_Red.png'),
	 ('1989',NULL,'2014-10-27','Taylor_Swift_-_1989.png'),
	 ('Reputation',NULL,'2017-11-10','Taylor_Swift_-_Reputation.png'),
	 ('Lover',NULL,'2019-08-23','Taylor_Swift_-_Lover.png'),
	 ('Folklore',NULL,'2020-07-24','Taylor_Swift_-_Folklore.png'),
	 ('Evermore',NULL,'2020-12-11','Taylor_Swift_-_Evermore.png'),
	 ('Midnights',NULL,'2022-10-21','Taylor_Swift_-_Midnights.png');
