const createConnectionPool = require('@databases/pg');
const { sql } = require('@databases/pg');

const pool = createConnectionPool(
    {
        connectionString: process.env.DATABASE_URL,
        bigIntMode: 'bigint',
        onQueryStart: (_query, { text, values }) => {
            console.log(
                `${new Date().toISOString()} START QUERY ${text} - ${JSON.stringify(
                    values,
                )}`,
            );
        },
        onQueryResults: (_query, { text }, results) => {
            console.log(
                `${new Date().toISOString()} END QUERY   ${text} - ${results.length
                } results`,
            );
        },
        onQueryError: (_query, { text }, err) => {
            console.log(
                `${new Date().toISOString()} ERROR QUERY ${text} - ${err.message}`,
            );
        },
    }
);

async function getTime() {
    const time = await pool.query(sql`SELECT NOW();`);
    return time;
}

async function getAllAlbums() {
    const albums = await pool.query(sql`SELECT * FROM albums ORDER BY release_date DESC;`);
    return albums
}


module.exports = {
    albums: {
        getAllAlbums
    },
    getTime,
    pool,
    sql
}

process.once('SIGTERM', () => {
    db.dispose().catch((ex) => {
        console.error(ex);
    });
});